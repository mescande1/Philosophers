/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   timeval_to_millisecond_long.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 11:33:20 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/23 18:16:56 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

long int	ttml(struct timeval h)
{
	long int	res;

	res = (h.tv_sec % 100000) * 1000000;
	res += h.tv_usec;
	return (res);
}

long int	timenow(void)
{
	struct timeval	h;

	gettimeofday(&h, NULL);
	return (ttml(h));
}
