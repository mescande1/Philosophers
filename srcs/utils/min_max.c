/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   min_max.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 10:52:34 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/28 10:53:40 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

long int	max(long int a, long int b)
{
	if (a > b)
		return (a);
	return (b);
}

long int	min(long int a, long int b)
{
	if (a < b)
		return (a);
	return (b);
}
