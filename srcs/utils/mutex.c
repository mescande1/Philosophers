/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 15:55:09 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/26 17:30:14 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int	mx_increment(t_mx *lock)
{
	pthread_mutex_lock(&lock->mx);
	lock->val++;
	pthread_mutex_unlock(&lock->mx);
	return (0);
}

long int	mx_get_val(t_mx *lock)
{
	long int	val;

	pthread_mutex_lock(&lock->mx);
	val = lock->val;
	pthread_mutex_unlock(&lock->mx);
	return (val);
}

int	mx_compare_and_swap(t_mx *lock, int old, int new)
{
	int	swap;

	pthread_mutex_lock(&lock->mx);
	swap = (old == lock->val);
	if (swap)
		lock->val = new;
	pthread_mutex_unlock(&lock->mx);
	return (swap);
}
