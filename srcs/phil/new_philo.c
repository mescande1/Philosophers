/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_philo.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/13 22:17:04 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/28 11:56:26 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

static int	do_think(t_info *args)
{
	long int		waiting_time;
	long int		mnow;

	mnow = timenow();
	waiting_time = max((args->data->eat_time * 2)
			- (mnow - args->lastmeal), 0) + 2000;
	if (mnow + waiting_time > args->lastmeal + args->data->die_time)
		waiting_time = 0;
	if (verif_dead(args))
		return (1);
	ft_action_write(mnow, args->n, 3, args);
	if (waiting_time)
		usleep(waiting_time);
	return (verif_dead(args));
}

static int	pasta_moment(t_info *args, int mealnum)
{
	long int	now;
	long int	meal_finished;

	now = timenow();
	args->lastmeal = now;
	ft_action_write(now, args->n, 1, args);
	meal_finished = args->lastmeal + args->data->eat_time;
	if (!verif_dead(args))
	{
		usleep(min(meal_finished - now,
				args->lastmeal + args->data->die_time - now) + 100);
		now = timenow();
	}
	if (now >= meal_finished && mealnum == args->data->mealnum)
		mx_increment(&args->data->mealsdone);
	return (verif_dead(args));
}

static int	digestion(t_info *args)
{
	long int	now;

	now = timenow();
	ft_action_write(now, args->n, 2, args);
	drop_forks(args);
	if (!verif_dead(args))
	{
		usleep(min(args->lastmeal + args->data->die_time - now,
				args->lastmeal + args->data->eat_time
				+ args->data->sleep_time - now) + 100);
		now = timenow();
	}
	return (verif_dead(args));
}

static int	actions(t_info *args, int mealnum)
{
	if (pick_forks(args))
		return (1);
	if (pasta_moment(args, mealnum))
		return (1);
	if (digestion(args))
		return (1);
	if (do_think(args))
		return (1);
	return (0);
}

t_info	*new_philo(t_info *args)
{
	int	mealnum;

	mealnum = 1;
	args->lastmeal = mx_get_val(&(args->data->start));
	mx_compare_and_swap(&args->status, STARTING, RUNNING);
	if (args->n % 2 == 0)
		usleep(args->data->eat_time * (1 + (args->n == args->data->philnum)));
	while (mx_get_val(&args->data->mealsdone) != args->data->philnum
		&& args->data->mealnum && !verif_dead(args))
	{
		if (actions(args, mealnum))
			break ;
		if (args->data->mealnum != -1)
			mealnum++;
	}
	while (mx_get_val(&args->status) != DEAD
		&& mx_get_val(&args->status) != DYINGCONFIRMED)
		usleep(1500);
	if (mx_get_val(&args->status) == DYINGCONFIRMED)
		pthread_mutex_unlock(&args->data->write);
	return (NULL);
}
