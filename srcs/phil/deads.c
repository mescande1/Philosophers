/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   deads.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 19:22:39 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/27 18:05:20 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	killme(t_info *args)
{
	pthread_mutex_lock(&args->data->write);
	if (!mx_compare_and_swap(&args->status, RUNNING, DYING))
		pthread_mutex_unlock(&args->data->write);
}

int	verif_dead(t_info *args)
{
	long int	now;

	if (mx_get_val(&args->status) >= DYING)
		return (1);
	now = timenow();
	if (now - args->lastmeal >= args->data->die_time)
	{
		killme(args);
		return (1);
	}
	return (0);
}
