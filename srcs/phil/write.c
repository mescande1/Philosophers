/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/19 17:24:57 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/27 19:50:24 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	ft_action_write(long int time, int philnum, int id, t_info *phil)
{
	long int	now;
	const char	*action[5] = {
		"has taken a fork",
		"is eating",
		"is sleeping",
		"is thinking"};

	(void)time;
	pthread_mutex_lock(&phil->data->write);
	pthread_mutex_lock(&phil->data->big_write);
	now = timenow();
	if (mx_get_val(&phil->status) < DYING)
		printf("%4.4ld %d %s\n", (now - mx_get_val(&phil->data->start)) / 1000,
			philnum, action[id]);
	pthread_mutex_unlock(&phil->data->big_write);
	pthread_mutex_unlock(&phil->data->write);
}
