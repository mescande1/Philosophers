/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   forks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 19:39:36 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/27 19:49:40 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int	pick_forks(t_info *args)
{
	while (!verif_dead(args))
	{
		if (mx_compare_and_swap(args->left, 0, 1))
		{
			if (mx_compare_and_swap(&args->right, 0, 1))
			{
				ft_action_write(timenow(), args->n, 0, args);
				ft_action_write(timenow(), args->n, 0, args);
				return (verif_dead(args));
			}
			else
				mx_compare_and_swap(args->left, 1, 0);
		}
		usleep(50);
	}
	return (1);
}

void	drop_forks(t_info *args)
{
	mx_compare_and_swap(args->left, 1, 0);
	mx_compare_and_swap(&args->right, 1, 0);
}
