/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_printers.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/13 15:53:23 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/13 15:55:46 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int	free_n_return(void *to_free, int to_return)
{
	free(to_free);
	return (to_return);
}

int	error_print(char *str, int val)
{
	dprintf(2, "Error :\n    %s\n", str);
	return (val);
}
