/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/13 16:09:58 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/25 23:59:28 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

static int	verif_max(char *str, int pos, char sign)
{
	const int	intmax[] = {2, 1, 4, 7, 4, 8, 3, 6, 4, 7};

	if (str[0] - '0' > intmax[pos] + (pos != 0 && sign == '-' && pos % 9 == 0))
		return (error_print("Out of range value", 1));
	if (str[0] - '0' == intmax[pos] + (pos != 0 && sign == '-' && pos % 9 == 0))
		if (str[1])
			return (verif_max(str + 1, pos + 1, sign));
	return (0);
}

int	verif_val(int ac, char **av)
{
	int	i;
	int	j;

	j = 1;
	while (j < ac)
	{
		i = -1;
		while (av[j][++i])
			if (!ft_isdigit(av[j][i]) && av[j][i] != '-')
				return (error_print("Non numeric value", 1));
		if (i > 10 || (i == 10 && verif_max(av[j] + (av[j][0] == '-'), 0, '-'))
				|| (j == 5 && av[j][0] == '-'))
			return (error_print("Out of range value", 1));
		j++;
	}
	return (0);
}

t_set	parse(int ac, char **av)
{
	t_set	res;

	memset(&res, 0, sizeof(t_set));
	if (verif_val(ac, av))
		return (res);
	res.philnum = ft_atoi(av[1]);
	if (res.philnum == 0)
	{
		error_print("Simulation can't work without Philosophers", 1);
		return (res);
	}
	res.die_time = ft_atoi(av[2]) * 1000;
	res.eat_time = ft_atoi(av[3]) * 1000;
	res.sleep_time = ft_atoi(av[4]) * 1000;
	if (ac == 6)
		res.mealnum = (unsigned int)ft_atoi(av[5]);
	else
		res.mealnum = -1;
	pthread_mutex_init(&(res.write), NULL);
	pthread_mutex_init(&(res.big_write), NULL);
	pthread_mutex_init(&(res.start.mx), NULL);
	pthread_mutex_init(&(res.mealsdone.mx), NULL);
	return (res);
}
