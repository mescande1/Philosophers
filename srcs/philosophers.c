/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/13 15:58:10 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/28 11:52:11 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

static void	set_philo_args(t_info *args, int i, t_set *datas)
{
	args[i].n = i;
	if (i != 0)
		args[i].left = &(args[i - 1].right);
	pthread_mutex_init(&(args[i].right.mx), NULL);
	pthread_mutex_init(&(args[i].status.mx), NULL);
	if (i == datas->philnum - 1)
		args[0].left = &(args[i].right);
	args[i].data = datas;
}

static int	verif_status(t_set *datas, t_info *philo_args)
{
	int		stop;
	int		i;

	stop = 1;
	i = 0;
	while (i < datas->philnum)
	{
		if (mx_get_val(&philo_args[i++].status) == DYING && stop)
		{
			pthread_mutex_lock(&datas->big_write);
			printf("%4.4ld %d died\n",
				(timenow() - mx_get_val(&datas->start)) / 1000, i - 1);
			stop = 0;
		}
	}
	return (stop);
}

static int	end_threads(t_set *datas, t_info *philo_args)
{
	int		i;
	int		stop;
	t_info	*return_val;

	stop = 1;
	i = 0;
	while (stop && mx_get_val(&datas->mealsdone) != datas->philnum)
	{
		i = 0;
		stop = verif_status(datas, philo_args);
		usleep(1000);
	}
	while (--i >= 0)
		if (!mx_compare_and_swap(&philo_args[i].status, RUNNING, DEAD))
			mx_compare_and_swap(&philo_args[i].status, DYING, DYINGCONFIRMED);
	if (!stop)
		pthread_mutex_unlock(&datas->big_write);
	while (++i < datas->philnum)
		pthread_join(philo_args[i].id_list, (void **)&return_val);
	free(philo_args);
	return (0);
}

int	philosophers(int ac, char **av)
{
	t_set			datas;
	int				i;
	t_info			*philo_args;

	datas = parse(ac, av);
	if (datas.philnum == 0)
		return (1);
	philo_args = ft_memalloc(sizeof(t_info) * datas.philnum);
	if (!philo_args)
		return (1);
	pthread_mutex_lock(&(datas.start.mx));
	i = 0;
	while (i < datas.philnum)
	{
		set_philo_args(philo_args, i, &datas);
		if (pthread_create(&(philo_args[i].id_list), NULL,
				(void *)new_philo, philo_args + i))
			return (error_print("Thread creation error",
					free_n_return(philo_args, 1)));
		i++;
	}
	datas.start.val = timenow();
	pthread_mutex_unlock(&datas.start.mx);
	return (end_threads(&datas, philo_args));
}

/*
 * TODO
 */
