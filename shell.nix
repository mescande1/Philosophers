with import <nixpkgs> {};
mkShell {
  buildInputs = [python3 python38Packages.bootstrapped-pip python38Packages.pip inotify-tools pkg-config valgrind ruby];
  shellHook =
   ''
     [ -d ".venv" ] || python3 -m venv .venv --prompt ".venv"
     source .venv/bin/activate
     python3 -m pip install --upgrade pip setuptools
     pip3 install norminette
     echo 'Shell started with the correct librairies'
     zsh
     exit
   '';
 }
