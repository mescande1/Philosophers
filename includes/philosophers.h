/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/13 15:17:30 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/28 10:54:15 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <sys/time.h>
# include <pthread.h>

# define DG(f,...) dprintf(9, "\033[31m%s\033[0m-\033[35m%d\033[0m: \033[33m" f "\033[0m\n", __func__, __LINE__, ##__VA_ARGS__)

typedef struct	s_protect_a_value {
	pthread_mutex_t		mx;
	long int			val;
}	t_mx;

typedef struct s_data_set_entry {
	int					philnum;
	int					die_time;
	int					eat_time;
	int					sleep_time;
	int					mealnum;
	t_mx				mealsdone;
	pthread_mutex_t		write;
	pthread_mutex_t		big_write;
	t_mx				start;
}	t_set;

enum phil_status {
	STARTING,
	RUNNING,
	DYING,
	DYINGCONFIRMED,
	DEAD
};

typedef struct s_information_for_philos {
	int			n;
	t_set		*data;
	t_mx		*left;
	t_mx		right;
	pthread_t	id_list;
	long int	lastmeal;
	t_mx		status;
}	t_info;

/*
 *Error_management
*/
int			free_n_return(void *to_free, int to_return);
int			error_print(char *str, int val);

/*
 *Parsing
*/
int			verif_val(int ac, char **av);
t_set		parse(int ac, char **av);

/*
 *Philo actions & usefull funcs
*/
void		killme(t_info *args);
int			verif_dead(t_info *args);
int		pick_forks(t_info *args);
void		drop_forks(t_info *args);

void		ft_action_write(long int time, int philnum, int id, t_info *phil);

long int	ttml(struct timeval h);
long int	timenow(void);

int			mx_increment(t_mx *lock);
int			mx_compare_and_swap(t_mx *lock, int old, int new);
long int	mx_get_val(t_mx *lock);

t_info		*new_philo(t_info *args);
int			philosophers(int ac, char **av);

/*
 *libft functions
*/
int			ft_atoi(const char *str);
int			ft_isdigit(int c);
void		*ft_memalloc(size_t len);
size_t		ft_strlen(const char *s);

long int	max(long int a, long int b);
long int	min(long int a, long int b);

/*
 *
*/

#endif
